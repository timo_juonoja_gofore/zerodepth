import express from "express"
import cors from "cors"
import bodyParser from "body-parser"

// create express application and middlewares
const app = express()
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// invalid json middleware
app.use((err, req, res, next) => {
  if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
    return res.sendStatus(400)
  }
  next()
})

// recursive function to find depths with zeros
const getDepthsWithZeros = (value, depthsWithZeros, depth = 0) => {
  if (Array.isArray(value)) {
    value.forEach(v => getDepthsWithZeros(v, depthsWithZeros, depth + 1))
  } else if (value === 0 && !depthsWithZeros.includes(depth)) {
    depthsWithZeros.push(depth)
  }
}

// zerodepth analyze helper function
const evaluateZeroDepths = (array) => {
  if (!Array.isArray(array)) {
    return [null, null]
  }

  const depthsWithZeros = []
  getDepthsWithZeros(array, depthsWithZeros)
  return [Math.min.apply(null, depthsWithZeros), Math.max.apply(null, depthsWithZeros)]
}

// POST zerodepth endpoint
app.post("/zerodepth", (req, res) => {
  res.send(evaluateZeroDepths(req.body))
})

// start application
app.listen(4321, () => console.log("Zerodepth app listening on port 4321."))
